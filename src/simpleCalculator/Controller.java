package simpleCalculator;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;

public class Controller {

                ObservableList<String> operator = FXCollections.observableArrayList(//
                        "/", "*", "-","+");

    @FXML
    TextField firstNumber;

    @FXML
    TextField secondNumber;

    @FXML
    Label result;

        @FXML
        Spinner<String> spinner = new Spinner<>();

    public void initialize(){
        SpinnerValueFactory<String> valueFactory =
                new SpinnerValueFactory.ListSpinnerValueFactory<>(operator);

        valueFactory.setValue("+");
        spinner.setValueFactory(valueFactory);
    }

    @FXML
    private void count(){
        double oneNumber = 0;
        double twoNumber = 0;
        try {
            oneNumber = Double.parseDouble(firstNumber.getText());
            twoNumber = Double.parseDouble(secondNumber.getText());
        }catch (Exception e){
            result.setText("Введите числа");
            return;
        }
        String operator =  spinner.getValue();

        double res;
        switch (operator) {
            case "+":  res = oneNumber+twoNumber;
                break;
            case "-":  res = oneNumber-twoNumber;
                break;
            case "*":  res = oneNumber*twoNumber;
                break;
            case "/":
                if(twoNumber==0) {
                result.setText("На ноль не делим");
                return;
            }else
                    res = oneNumber/twoNumber;
                break;
            default: res = 0;
                break;
        }
        result.setText(String.valueOf(res));
    }
}
