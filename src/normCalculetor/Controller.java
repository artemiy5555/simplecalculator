package normCalculetor;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;


public class Controller {

    String str;

    @FXML
    private TextField workingPanel;
    @FXML
    private Label resultPanel;

    @FXML private void numOne(){
        workingPanel.appendText("1");
    }
    @FXML private void numTwo(){
        workingPanel.appendText("2");
    }
    @FXML private void numThr(){
        workingPanel.appendText("3");
    }
    @FXML private void numFour(){
        workingPanel.appendText("4");
    }
    @FXML private void numFive(){
        workingPanel.appendText("5");
    }
    @FXML private void numSix(){
        workingPanel.appendText("6");
    }
    @FXML private void numSeven(){
        workingPanel.appendText("7");
    }
    @FXML private void numEig(){
        workingPanel.appendText("8");
    }
    @FXML private void numNine(){
        workingPanel.appendText("9");
    }
    @FXML private void numPoint(){
        workingPanel.appendText(".");
    }
    @FXML private void numZero(){
        workingPanel.appendText("0");
    }
    @FXML private void numDel(){
        workingPanel.appendText("/");
    }
    @FXML private void numMulti(){
        workingPanel.appendText("*");
    }
    @FXML private void numProz(){
        workingPanel.appendText("%");
    }
    @FXML private void numMinus(){
        workingPanel.appendText("-");
    }
    @FXML private void numAdd(){
        workingPanel.appendText("+");
    }
    @FXML private void numLefttBracket(){
        workingPanel.appendText("(");
    }
    @FXML private void numReghtBracket(){
        workingPanel.appendText(")");
    }
    @FXML private void numLefttTag(){
        workingPanel.appendText("<");
    }
    @FXML private void numReghtTag(){
        workingPanel.appendText(">");
    }
    @FXML private void numDeleteAll(){
        workingPanel.setText("");
    }

    @FXML private  void numDelete(){

            str=workingPanel.getText();
            str = str.substring(0, workingPanel.getText().length() - 1);
            workingPanel.setText(str);
    }

    @FXML
    private  void numSmooth(){

        Calculator calculator = new Calculator();
        String result = calculator.calculate(workingPanel.getText()).replace(".00","");
        resultPanel.setText(workingPanel.getText()+"=");
        workingPanel.setText(result);
    }
}
